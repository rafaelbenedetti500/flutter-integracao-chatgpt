import 'dart:convert';
import 'dart:io';

import 'package:avatar_glow/avatar_glow.dart';
import 'package:dart_openai/openai.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_application_1/components/appbar.dart';
import 'package:flutter_application_1/mobx/home_page_mobx.dart';
import 'package:flutter_application_1/services/command_analyzer.dart';
import 'package:flutter_application_1/services/speech_service.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:intl/date_symbol_data_local.dart';
import 'package:mime/mime.dart';
import 'package:open_filex/open_filex.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final homeMobx = GetIt.I.get<HomeVariablesController>();

  List<types.Message> _messages = [];
  String texta = '';
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
  }

  void sendToChatGpt(String content) async {
    setState(() {
      isLoading = true;
    });
    Stream<OpenAIStreamChatCompletionModel> chatStream =
        OpenAI.instance.chat.createStream(
      model: "gpt-3.5-turbo",
      maxTokens: 1000,
      n: 1,
      temperature: 0.1,
      topP: 1,
      messages: [
        OpenAIChatCompletionChoiceMessageModel(
          content: content,
          role: OpenAIChatMessageRole.user,
        )
      ],
    );
    String text = '';
    chatStream.listen((chatStreamEvent) async {
      chatStreamEvent.choices.forEach((element) {
        if (element.delta.content != null) {
          texta = text += element.delta.content!;
          final textMessage = types.TextMessage(
            author: types.User(id: 'ChatGPT', firstName: 'ChatGPT'),
            createdAt: DateTime.now().millisecondsSinceEpoch,
            id: 'Uuid().v4()',
            text: texta,
          );
          _addMessage(textMessage);
        }
      });
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: const TopAppBar(),
        body: Stack(
          children: [
            Observer(
              builder: (_) => Chat(
                key: UniqueKey(),
                customBottomWidget: Container(
                  decoration: BoxDecoration(
                      color: Color(0xff151521),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  height: 50,
                  width: double.infinity,
                  child: Column(
                    children: [
                      TextField(
                        readOnly: isLoading,
                        style: TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontSize: 14,
                            fontFamily: 'Comfortaa',
                            fontWeight: FontWeight.bold),
                        cursorWidth: 1,
                        cursorColor: Color.fromARGB(255, 255, 255, 255),
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(top: 16),
                          enabledBorder: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          hintStyle: TextStyle(
                            color: Color.fromARGB(255, 202, 202, 202),
                            fontSize: 12,
                            fontFamily: 'Comfortaa',
                          ),
                          hintText: 'Fale com o chatGPT',
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          labelStyle: TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontSize: 14,
                            fontFamily: 'Comfortaa',
                          ),
                          suffixIcon: isLoading
                              ? Padding(
                                  padding: EdgeInsets.only(right: 8),
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                )
                              : AvatarGlow(
                                  endRadius: 20,
                                  glowColor: Colors.white,
                                  animate: homeMobx.isTopMicListening,
                                  child: IconButton(
                                      color: Colors.white,
                                      onPressed: () {
                                        String text = '';
                                        _startListen(text, context);
                                      },
                                      icon: Icon(Icons.mic)),
                                ),
                          prefixIcon: Icon(Icons.search,
                              size: 20,
                              color: Color.fromARGB(255, 255, 255, 255)),
                        ),
                        onSubmitted: (value) {
                          final textMessage = types.TextMessage(
                            author: types.User(id: 'Você', firstName: 'Você'),
                            createdAt: DateTime.now().millisecondsSinceEpoch,
                            id: 'Uuid().v4()',
                            text: value,
                          );
                          sendToChatGpt(value);

                          _addMessage(textMessage);
                        },
                      ),
                    ],
                  ),
                ),
                messages: _messages,
                onMessageTap: _handleMessageTap,
                onPreviewDataFetched: _handlePreviewDataFetched,
                onSendPressed: (nada) {},
                showUserAvatars: true,
                inputOptions: InputOptions(),
                showUserNames: true,
                avatarBuilder: (user) {
                  return (CircleAvatar(
                    backgroundColor:
                        user == 'ChatGPT' ? Colors.purple : Colors.blue,
                    child: Text(
                      user,
                      style: TextStyle(color: Colors.white, fontSize: 10),
                    ),
                  ));
                },
                user: types.User(id: 'a'),
              ),
            ),
          ],
        ),
      );

  void _addMessage(types.Message message) {
    setState(() {
      if (_messages.isNotEmpty) {
        var lastMessage = _messages[0];
        if (lastMessage.author.id == message.author.id) {
          _messages[0] = message;
          return;
        }
      }
      _messages.insert(0, message);
    });
  }

  Future<void> _startListen(String text, BuildContext context) async {
    await homeMobx.initSpeech();
    homeMobx.atualizaTopMicIsListening(true);
    await Future.delayed(Duration(milliseconds: 500));

    homeMobx.atualizaTopMicIsListening(true);
    SpeechService.listen(
      onResult: (value) async {
        text = value;
        if (text.isNotEmpty) {
          var string = await CommandAnalyzer.analize(
            context: context,
            request: text,
          );
          if (string.isEmpty) {
            homeMobx.atualizaTopMicIsListening(false);
          } else {
            final textMessage = types.TextMessage(
              author: types.User(id: 'Você', firstName: 'Você'),
              createdAt: DateTime.now().millisecondsSinceEpoch,
              id: 'Uuid().v4()',
              text: string,
            );
            _addMessage(textMessage);
            sendToChatGpt(text);
          }
        }
      },
    );
  }

  void _handleMessageTap(BuildContext _, types.Message message) async {
    if (message is types.FileMessage) {
      var localPath = message.uri;

      if (message.uri.startsWith('http')) {
        try {
          final index =
              _messages.indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (_messages[index] as types.FileMessage).copyWith(
            isLoading: true,
          );

          setState(() {
            _messages[index] = updatedMessage;
          });

          final client = http.Client();
          final request = await client.get(Uri.parse(message.uri));
          final bytes = request.bodyBytes;

          if (!File(localPath).existsSync()) {
            final file = File(localPath);
            await file.writeAsBytes(bytes);
          }
        } finally {
          final index =
              _messages.indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (_messages[index] as types.FileMessage).copyWith(
            isLoading: null,
          );

          setState(() {
            _messages[index] = updatedMessage;
          });
        }
      }

      await OpenFilex.open(localPath);
    }
  }

  void _handlePreviewDataFetched(
    types.TextMessage message,
    types.PreviewData previewData,
  ) {
    final index = _messages.indexWhere((element) => element.id == message.id);
    final updatedMessage = (_messages[index] as types.TextMessage).copyWith(
      previewData: previewData,
    );

    setState(() {
      _messages[index] = updatedMessage;
    });
  }
}
