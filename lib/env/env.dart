// lib/env/env.dart
import 'package:envied/envied.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;


@Envied(path: ".env")
abstract class Env {
  @EnviedField(varName: 'OPEN_AI_API_KEY') // the .env variable.
   static var apiKey = DotEnv.dotenv.env['OPEN_AI_API_KEY'];
}
