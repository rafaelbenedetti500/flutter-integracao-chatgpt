import 'package:dart_openai/openai.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/mobx/home_page_mobx.dart';
import 'package:flutter_application_1/views/home/home_page.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;

import 'env/env.dart';

void main() async {
  GetIt getIt = GetIt.I;
  getIt.registerSingleton<HomeVariablesController>(HomeVariablesController());
  await DotEnv.dotenv.load(fileName: ".env");
  OpenAI.apiKey = Env.apiKey!; // Initializes the package with that API key
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}
