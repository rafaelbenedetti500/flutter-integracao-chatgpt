import 'package:flutter/material.dart';

class TopAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;

  const TopAppBar({
    Key? key,
  })  : preferredSize = const Size.fromHeight(70.0),
        super(key: key);

  @override
  State<TopAppBar> createState() => _TopAppBarState();
}

class _TopAppBarState extends State<TopAppBar> {
  bool permissao = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _platform();
  }

  _platform() {
    return AppBar(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10))),
      toolbarHeight: 90,
      elevation: 16,
      backgroundColor: Color(0xff151521),
      centerTitle: true,
      title: Text('Chat Integrado com ChatGPT')
    );
  }
}
