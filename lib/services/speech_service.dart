import 'package:flutter_application_1/mobx/home_page_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:speech_to_text/speech_to_text.dart';

class SpeechService {
  static final _speechToText = SpeechToText();

  static Future listen({
    required Function(String text) onResult,
  }) async {
    final homeMobx = GetIt.I.get<HomeVariablesController>();

    if (!_speechToText.isListening) {
      await _speechToText.listen(
          onResult: ((result) {
            if (!_speechToText.isListening) {
              onResult(result.recognizedWords);
            }
          }),
          localeId: 'pt-br',
          pauseFor: Duration(seconds: 3));
      await Future.delayed(Duration(seconds: 4));
      await homeMobx.atualizaTopMicIsListening(false);
    } else {
      _speechToText.stop();
    }
  }
}
