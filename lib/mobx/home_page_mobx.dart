import 'package:mobx/mobx.dart';
import 'package:speech_to_text/speech_to_text.dart';

part 'home_page_mobx.g.dart';

class HomeVariablesController = _HomeVariablesController
    with _$HomeVariablesController;

abstract class _HomeVariablesController with Store {

  @observable
  SpeechToText speechToText = SpeechToText();

  @observable
  bool isTopMicListening = false;

  @action
  atualizaTopMicIsListening(mic) {
    isTopMicListening = mic;
  }

  @action
  initSpeech() {
    speechToText.initialize();
  }
}
