// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_page_mobx.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$HomeVariablesController on _HomeVariablesController, Store {
  late final _$speechToTextAtom =
      Atom(name: '_HomeVariablesController.speechToText', context: context);

  @override
  SpeechToText get speechToText {
    _$speechToTextAtom.reportRead();
    return super.speechToText;
  }

  @override
  set speechToText(SpeechToText value) {
    _$speechToTextAtom.reportWrite(value, super.speechToText, () {
      super.speechToText = value;
    });
  }

  late final _$isTopMicListeningAtom = Atom(
      name: '_HomeVariablesController.isTopMicListening', context: context);

  @override
  bool get isTopMicListening {
    _$isTopMicListeningAtom.reportRead();
    return super.isTopMicListening;
  }

  @override
  set isTopMicListening(bool value) {
    _$isTopMicListeningAtom.reportWrite(value, super.isTopMicListening, () {
      super.isTopMicListening = value;
    });
  }

  late final _$_HomeVariablesControllerActionController =
      ActionController(name: '_HomeVariablesController', context: context);

  @override
  dynamic atualizaTopMicIsListening(dynamic mic) {
    final _$actionInfo = _$_HomeVariablesControllerActionController.startAction(
        name: '_HomeVariablesController.atualizaTopMicIsListening');
    try {
      return super.atualizaTopMicIsListening(mic);
    } finally {
      _$_HomeVariablesControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic initSpeech() {
    final _$actionInfo = _$_HomeVariablesControllerActionController.startAction(
        name: '_HomeVariablesController.initSpeech');
    try {
      return super.initSpeech();
    } finally {
      _$_HomeVariablesControllerActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
speechToText: ${speechToText},
isTopMicListening: ${isTopMicListening}
    ''';
  }
}
